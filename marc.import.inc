<?php

function marc_import_file($filename, $iid = null){
  if (!$iid){
    $iid = db_result(db_query("SELECT iid FROM {marc_import} ORDER BY iid DESC LIMIT 1"));
  }
  $marc_records = marc_get_contents_utf8($filename);

  //http://www.loc.gov/marc/specifications/specrecstruc.html
  //MARC record terminator is 1D(hex) or Decimal 29, Oct 035, Character GS
  //Not using File_MARC here as it seems a bit unnecessary, just breaking file up in to individual records.
  $marc_records = explode(chr(29), $marc_records);
  $count = 0;
  foreach ($marc_records as $marc_record) {
    if (strlen($marc_record) > 12) { 
      $marc->iid = $iid;
      $marc->raw = $marc_record;
      $marc->created = 0;
      drupal_write_record('marc', $marc);
      $marc = null;
      $count++;
    }
  }
  return $count;
}

function marc_get_contents_utf8($fn){ 
  $content = file_get_contents($fn); 
  return mb_convert_encoding($content, 'UTF-8', mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true)); 
} 

function marc_process(&$marc, $iid, $data = array()){
  $marc->iid = $iid;
  if (variable_get('marc_library_file_marc', FALSE)){
    module_load_include('inc', 'marc', 'marc.file_marc');
    $marc->record = marc_filemarc_new($marc);
  }
  if (variable_get('marc_library_legacy', FALSE)){
    module_load_include('inc', 'marc', 'marc.legacy');
    $marc->legacy = marc_legacy_record($marc);
  }
  $marc->created = time();
  module_invoke_all('marcapi', 'presave', $marc, $data);
  drupal_write_record('marc', $marc, 'mid');
  module_invoke_all('marcapi', 'save', $marc, $data);
  //cache_set($marc->mid, $marc, 'cache_marc', CACHE_TEMPORARY);
}