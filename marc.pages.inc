<?php

function marc_view_record($mid) {
  $marc = marc_load_marc(array('mid' => $mid));
  if (is_array($marc->legacy)){
  $fields = $marc->legacy;
  drupal_set_title($fields['245'][0]['a']);
  if ($fields['leader']){
    $row[] = array('leader', null, null, $fields['leader']);
  }
  if ($fields['007']){
    $row[] = array('007', null, null, $fields['007']);
  }
  if ($fields['008']){
    $row[] = array('008', null, null, $fields['008']);
  }
  ksort($fields);
  foreach ($fields AS $field => $iterations) {
    if (is_array($iterations)) {
      foreach ($iterations AS $iteration => $subfield_data) {

        if (is_array($subfield_data)) {
          $i1 = $subfield_data[1];
          $i2 = $subfield_data[2];
          $subfields = array_slice($subfield_data, 2);
          ksort($subfields);
          foreach ($subfields AS $subfield => $value) {
            $row[] = array($field, $i1 . $i2, $subfield, check_plain($value));
          }
        }
      }
    }
  }
  if (count($row) > 0){
    $header = array('Field', ' ', 'Subfield', 'Value');
  }
  else {
    drupal_set_message(t('No MARC record found'));
  }

  //This needs a real theme function at some point
  $output = theme('table', $header, $row);
  }
  else {
    $output .= 'You need to enable support for the Original MARC Library';
  }
  return $output;
}