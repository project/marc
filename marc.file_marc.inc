<?php
require 'File/MARC.php';

function marc_filemarc_new($marc) {
  $records = new File_MARC($marc->raw, File_MARC::SOURCE_STRING);
  $record = $records->next();
  return $record;
}
