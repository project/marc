<?php

/** 
 * Define the settings form. 
 */

function marcnode_settings() {

  $form = array();
  $null = array(null);
  $options = array(0 => t('Do not map')) + marcnode_bib_options();
  
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('MARC Content Types'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('These content types will be available to be created with MARC imports'),
    );
  
  $types = marcnode_content_types();
  
  foreach ($types as $type => $name){
    $form['types']['marcnode_'.$type.'_status'] = array(
      '#type' => 'checkbox', 
      '#title' => $name,
      '#default_value' => variable_get('marcnode_'.$type.'_status', FALSE),
    );
  }
  
  foreach ($types as $type => $name){
    
    if (variable_get('marcnode_'.$type.'_status', FALSE)){
      
      $form[$type] = array(
        '#type' => 'fieldset',
        '#title' => $name,
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        );

      $form[$type]['marcnode_'.$type.'_title'] = array(
        '#type' => 'select',
        '#title' => t('Title'),
        '#options' => $options,
        '#default_value' => variable_get('marcnode_'.$type.'_title', null),
        );
      $form[$type]['marcnode_'.$type.'_body'] = array(
        '#type' => 'select',
        '#title' => t('Body'),
        '#options' => $options,
        '#default_value' => variable_get('marcnode_'.$type.'_body', null),
        );
      if (module_exists('content')){
        $content = content_types($type);
        //dpm($content);
        foreach ($content['fields'] as $field_name => $field){
          $label = $field['widget']['label'] ? $field['widget']['label'] : $field_name;
          foreach($field['columns'] as $column_name => $column){
            $label = count($field['columns']) > 1 ? $label.'('.$column_name.')' : $label;
            $form[$type]['marcnode_'.$type.'_'.$field_name.'_'.$column_name] = array(
              '#type' => 'select',
              '#title' => $label,
              '#options' => $options,
              '#default_value' => variable_get('marcnode_'.$type.'_'.$field_name.'_'.$column_name, null),
            );
          }
        }
      }
      if (module_exists('taxonomy')){
        $vocabularies = marcnode_vocabularies($type);
        foreach ($vocabularies as $vid => $name){
          $form[$type]['marcnode_'.$type.'_'.$vid] = array(
            '#type' => 'select',
            '#title' => $name,
            '#options' => $options,
            '#default_value' => variable_get('marcnode_'.$type.'_'.$vid, null),
            );
        }
      }
    }
  }
  
  return system_settings_form($form); 
}


