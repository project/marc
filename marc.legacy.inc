<?php

// This is the key utility for breaking a marc record in to a usable array
// This function is getting deprecated in favor of using File_MARC

function marc_legacy_record($marc) {
  $marc_record = $marc->raw;
  
  // Just want to get marc fields out of MARC format and in to an usable array
  // http://www.loc.gov/marc/specifications/specrecstruc.html
  
  // Split record in to fields
  
  // field terminator
  // FT 1E(hex), 30(Dec), 036(Oct), RS(Char)
  
  
  // Create array by exploding the marc records
  $marc_field_values = explode(chr(30), $marc_record);
  // Now this is harder, we need to break the leader from the directory
  $start_length = strlen($marc_field_values[0]);
  $leader = substr($marc_field_values[0], 0, 23);
  $directory = substr($marc_field_values[0], 24, $start_length);
  $marc_field_values[0] = $leader;
 
  // Get the field numbers from the directory
  // $directoryfields contaings the fieldname, start position and length
  // that will get taken care of in a second
  $directory_fields = str_split($directory, 12);
  
  //Start building $record array
  $record = array();
  //First we set the leader and take the leader value out of the values
  $record['leader'] = $leader;
  array_splice($marc_field_values, 0, 1);

  //Then we loop through the directory fields and build array
  foreach ($directory_fields AS $key => $directory_field) {
    
    //The marc field number is just the first 3 characters
    $field_number = substr($directory_field, 0, 3);
    //The field value is the correpsonding value in the marcfieldvalues array.
    $field_value = $marc_field_values[$key];
    
    //We need to keep track of the field iterations
    if (!$marc_field_count[$field_number]){
      $marc_field_count[$field_number] = 0;
    }
    $field_count = $marc_field_count[$field_number];
    if (substr($directory_field, 0, 2) == '00') {
      // Populate Control fields
      // Technically control fields can be repeated, not that I ever see it.
      $record[$field_number][$field_count] = $field_value;
    }
    else {
      //Populate Indicators
      //$record[$field_number][$field_count]['field'] = $field_value;
      $record[$field_number][$field_count]['i1'] = substr($field_value, 0, 1);
      $record[$field_number][$field_count]['i2'] = substr($field_value, 1, 1);
      
      //Start work on subfields
      // US (char) 31(dec) 1F 037 - character used to seperate subfields
      $subfields = explode(chr(31), $marc_field_values[$key]);
      
      //Get rid of indicators
      array_splice($subfields, 0, 1);

       //$output .= $subfields[0]. '<br>';      

      //Insert subfields in database
      foreach ($subfields as $subfield) {
        $subfield_code = substr($subfield, 0, 1);
        $subfield_value = substr($subfield, 1, (strlen($subfield)-1));
        $record[$field_number][$field_count][$subfield_code] = $subfield_value;
      }
    }
    //We need to keep track of the field iterations
    $marc_field_count[$field_number]++;
  }
  return $record;
}
