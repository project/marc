<?php

/** 
 * Define the settings form. 
 */

function marc_admin_settings() {
  module_load_include('inc', 'marc', 'marc.field');
  global $user;
  if (module_exists('content')){
    $message = t('To map your MARC records to more than the title and author field,
      visit the '. l(t('Content types'), 'admin/content/types').' page and select the
      content type that you will be importing MARC records to and then select the MARC tab.');
  }
  else {
    $message = t('By default, you can import MARC records in to the title, body fields and taxonomy fields
      of your content.');
  }
  $form['file'] = array(
    '#type' => 'fieldset',
    '#title' => t('File Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -6,
    );
    
  $form['file']['marc_file_directory_path'] = array(
    '#type' => 'textfield', 
    '#title' => t('Path to your MARC files'), 
    '#description' => t('If blank, it will use your default files directory path.'),
    '#default_value' => variable_get('marc_file_directory_path', file_directory_path()),
  );

  $form['file']['marc_file_extensions'] = array(
    '#type' => 'textfield', 
    '#title' => t('Valid MARC file extensions'), 
    '#description' => t('Separate extensions with a single space.'),
    '#default_value' => variable_get('marc_file_extensions', 'mrc mrk 001'),
  );
  
$form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -2,
    );
    
  $form['cron']['marc_cron_limit'] = array(
    '#type' => 'textfield', 
    '#title' => t('Records per Cron'), 
    '#description' => t('The number of records to process during each cron.'),
    '#default_value' => variable_get('marc_cron_limit', FALSE),
  );
  
  $form['libraries'] = array(
    '#type' => 'fieldset',
    '#title' => t('MARC Libraries'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -2,
    '#description' => 'Choose a MARC library to use to read your records. You may choose more than one.',
    );
    
  $form['libraries']['marc_library_file_marc'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Use PEAR File_MARC Library'), 
    '#description' => t('This is a pear extension for php, which you may need to install first.'),
    '#default_value' => variable_get('marc_library_file_marc', FALSE),
  );

  $form['libraries']['marc_library_legacy'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Use legacy MARC library.'), 
    '#description' => t('This is the original library used by the MARC module and does not require any addition php extensions.'),
    '#default_value' => variable_get('marc_library_legacy', TRUE),
  );
  return system_settings_form($form); 
}

